#!/usr/bin/python
#
# test file for mond
#
# Copyright (c) Retotech AB, 2016-2017 
# 
# Authors: 
# Ola Redell <ola.redell@retotech.se> 
# 
# This work is licensed under the terms of the MIT license. See 
# the COPYING file in the top-level directory.


import time
import Queue
import argparse
from timeit import default_timer as timer
from time import sleep

import socket
import sys
import os

server_address = '/tmp/icctd_socket'

# Make sure the socket does not already exist
try:
    os.unlink(server_address)
except OSError:
    if os.path.exists(server_address):
        raise
        

# Create a UDS socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print >>sys.stderr, 'waiting for a connection'
    connection, client_address = sock.accept()
    try:
        print >>sys.stderr, 'connection from', client_address

        # Send strings of various lengths
        while True:
            data = "segway_angle:df7894.rr" + '\0'
            print >>sys.stderr, 'sent "%s"' % data
	    connection.sendall(data)
            sleep(1)
            data = "segway_angle: 7894.7 " + '\0'
            print >>sys.stderr, 'sent "%s"' % data
	    connection.sendall(data)
            sleep(1)
            data = "segway_angle:7894.779785" + '\0'
            print >>sys.stderr, 'sent "%s"' % data
	    connection.sendall(data)
            sleep(1)
            data = "END:now getting quiet" + '\0'
            print >>sys.stderr, 'sent "%s"' % data
	    connection.sendall(data)
            sleep(1)
                        
    finally:
        # Clean up the connection
        connection.close()

