#
# makefile for mond
# Original Makefile taken from https://github.com/mbcrawfo/GenericMakefile
#
# Copyright (c) 2014 Michael Crawford
# Copyright (c) 2016-2017 Retotech AB
#
#### PROJECT SETTINGS ####
#
# The name of the executable to be created
BIN_NAME := mond

# Compiler used
CC ?= gcc

# Extension of source files used in the project
SRC_EXT = c

# Path to the source directory, relative to the makefile
SRC_PATH = src

# Space-separated pkg-config libraries used by this project
LIBS = 

# General compiler flags
COMPILE_FLAGS = -Wall -Wextra -g

# Add additional include paths
INCLUDES = -I $(SRC_PATH) -I ./streamd-src

# General linker settings
LINK_FLAGS = -pthread 

#### END PROJECT SETTINGS ####

# Generally should not need to edit below this line

# Function used to check variables. Use on the command line:
# make print-VARNAME
# Useful for debugging and adding features
print-%: ; @echo $*=$($*)

# Clear built-in rules
.SUFFIXES:

# Append pkg-config specific libraries if need be
ifneq ($(LIBS),)
	COMPILE_FLAGS += $(shell pkg-config --cflags $(LIBS))
	LINK_FLAGS += $(shell pkg-config --libs $(LIBS))
endif

# Verbose option, to output compile and link commands
export V := true
export CMD_PREFIX := @
ifeq ($(V),true)
	CMD_PREFIX :=
endif

# Combine compiler and linker flags
CFLAGS ?= $(COMPILE_FLAGS) 
LDFLAGS ?= $(LINK_FLAGS) 

# Build and output paths
BUILD_PATH := build
BIN_PATH := bin

# Find all source files in the source directory, sorted by most
# recently modified
SOURCES = $(shell find $(SRC_PATH) -name '*.$(SRC_EXT)' \
	-printf '%T@\t%p\n' | sort -k 1nr | cut -f2-)

# Set the object file names, with the source directory stripped
# from the path, and the build path prepended in its place
OBJECTS = $(SOURCES:$(SRC_PATH)/%.$(SRC_EXT)=$(BUILD_PATH)/%.o)

# Set the dependency files that will be used to add header dependencies
DEPS = $(OBJECTS:.o=.d)


# Release is default target
.PHONY: all
all: dirs symlink


# Debug build for gdb debugging
.PHONY: debug
debug: dirs symlink


# Create the directories used in the build
.PHONY: dirs
dirs:
	@echo "Creating directories"
	@mkdir -p $(dir $(OBJECTS))
	@mkdir -p $(BIN_PATH)

# Removes all build files
.PHONY: clean
clean:
	@echo "Deleting $(BIN_NAME) symlink"
	@$(RM) $(BIN_NAME)
	@echo "Deleting directories"
	@$(RM) -r build
	@$(RM) -r bin

# Main rule, checks the executable and symlinks to the output
symlink: $(BIN_PATH)/$(BIN_NAME)
	@echo "Making symlink: $(BIN_NAME) -> $<"
	@$(RM) $(BIN_NAME)
	@ln -s $(BIN_PATH)/$(BIN_NAME) $(BIN_NAME)

# Link the executable
$(BIN_PATH)/$(BIN_NAME): $(OBJECTS)
	@echo "Linking: $@"
	$(CMD_PREFIX)$(CC) $(OBJECTS) $(LDFLAGS) -o $@

# Add dependency files, if they exist
-include $(DEPS)

# Source file rules
# After the first compilation they will be joined with the rules from the
# dependency files to provide header dependencies
$(BUILD_PATH)/%.o: $(SRC_PATH)/%.$(SRC_EXT)
	@echo "Compiling: $< -> $@"
	$(CMD_PREFIX)$(CC) $(CFLAGS) $(INCLUDES) -MP -MMD -c $< -o $@

