/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#include <dirent.h>
#include <string.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#include "mond.h"
#include "procfile.h"
#include "message.h"

void do_work(struct mond * m);
void sig_handler(int signo);
void mond_teardown(struct mond * m);
void perform_proc_readings(struct mond * m, unsigned long uptime);
void update_sys_util(struct utilization * u);
unsigned long mond_get_uptime();
void generate_status_file(struct mond * m);

static struct mond * _mond;

/*
 * Initialize mond
 */
void mond_init(struct mond * m) {
//	printf("mond_init\n");

	m->process = NULL;
	m->refresh_period = 3000000;
	m->quit_threads = 0;

	// socket reset
	socket_init(&m->socket, "/tmp/icctd_socket");

	// messages and freertos vals
	strcpy(m->cut_msg, "");
	m->segway_angle = 0.0;

	m->util.first_call = 1;
	m->util.sys_hertz = sysconf(_SC_CLK_TCK);
	mond_set_output_status_file(m, "/tmp/mond-util");

	_mond = m;

	if (signal(SIGINT, sig_handler) == SIG_ERR)
		printf("failed to register signal handler for SIGINT\n");

	if (signal(SIGTERM, sig_handler) == SIG_ERR)
		printf("failed to register signal handler for SIGTERM\n");
}


void mond_set_sockpath(struct mond * m, char * sockpath)
{
	printf("mond_set_sockpath %s\n", sockpath);

	socket_init(&m->socket, sockpath);
}

/*
 * Start mond
 */
void mond_start(struct mond * m) {
//	printf("mond_start\n");

	do_work(m);
}


void mond_handle_messages(struct mond * m, char * strbuf, int nread)
{
	char msg[2*MAX_MSG_LEN];
	int len;
	char * bufp;

	// mark end of received data with null character
	strbuf[nread] = '\0';
//	printf("handle_messages received: %s\n", strbuf);

	/* If there is anything left from an earlier socket reading
	 * that did not make a full message (in cut_msg), use it to prepend
	 * the new data. Otherwise, the msg buffer is initialized with
	 * an empty string (cut_msg is empty).
	 */
	strcpy(msg, m->cut_msg);

	strcat(msg, strbuf);
	len = strlen(strbuf);

	/* Check for unlikely case that all data in strbuf is one incomplete message,
	 * If it holds just one complete message (which is more likely) it's length
	 * would be nread-1
	 */
	if (len == nread)
	{
		if (strlen(msg) > MAX_MSG_LEN-1)
		{
			printf("mond received too long message %s (%d)\n", msg, strlen(msg));
			strcpy(m->cut_msg, "");
			return;
		}
		/* message is incomplete but within size bounds, save it for later
		 */
		strcpy(m->cut_msg, msg);
		return;
	}

	/* Go through all messages in [cut_msg + strbuf] and deal with them
	 * There may be an incomplete one in the end */
	bufp = strbuf;
	do {
		mond_act_on_message(m, msg);
		bufp += len + 1;
		strcpy(msg, bufp);
		len = strlen(bufp);
	} while (len > 0 && \
			(bufp + len < strbuf + nread));

	/* If the last string in strbuf terminates at (strbuf+nread)
	 * then it is an incomplete message, save for later */
	if (len > 0)
	{
		strcpy(m->cut_msg, msg);
		printf("handle_messages got a remainder for next round: [%s]\n", msg);
	}
	else
		strcpy(m->cut_msg, "");
}


int mond_act_on_message(struct mond * m, char * msg)
{
//	printf("act on:  [%s]\n", msg);

	char * tag = strtok(msg, ":");

	if (tag == NULL)
		return ERROR;

	switch (message_id_from_tag(tag)) {
	case ANGLE:
	{
		char * fstr = strtok(NULL,":");
		long l;

		if (fstr == NULL)
		{
			printf("mond got ill formatted message '%s'\n", msg);
			return ERROR;
		}

		l = strtol(fstr, NULL, 10);

		m->segway_angle = l/1000.0;
//		printf("got angle and %s - %f\n", fstr, f);
		break;
	}
	case BADTAG:
		printf("mon got invalid message '%s'\n", msg);
		return ERROR;
	}

	return OK;
}


void do_work(struct mond * m)
{
	unsigned long uptime;
	char strbuf[MAX_MSG_LEN+1];
	int nread = 0;

	//for (int i = 0; i < 3; i++)
	while (!m->quit_threads)
	{
		if (socket_status(&m->socket) == SOCK_CONNECTED || \
			socket_connect(&m->socket) == SOCK_CONNECTED)
		{
			nread = socket_receive(&m->socket, strbuf, MAX_MSG_LEN);
			if (nread > 0)
			{
				mond_handle_messages(m, strbuf, nread);
			}
		}

		uptime = mond_get_uptime();
		perform_proc_readings(m, uptime);

		generate_status_file(m);

		usleep(m->refresh_period);
	}
}


unsigned long mond_get_uptime()
{
	char uptime_str[30];
	unsigned long uptime = 0;
	struct procfile pf;

	if (procfile_init(&pf, "/proc/uptime") == ERROR)
		return 0;

	if (procfile_get_field_from_line(&pf, 1, uptime_str) == OK)
		uptime = atol(uptime_str);

	procfile_cleanup(&pf);

	return uptime;
}

int mond_add_process(struct mond * m, int pid)
{
	struct process * p = (struct process *) malloc(sizeof(struct process));
	if (process_init(p, pid) == OK)
	{
		p->next = m->process;
		m->process = p;
		return OK;
	}
	else
	{
		free(p);
		printf("ERROR: Failed to create process representation for %d\n", pid);
		return ERROR;
	}
}

int mond_add_process_by_pidstr(struct mond * m, char * pidstr)
{
	int pid = atoi(pidstr);
	return mond_add_process(m, pid);
}

int mond_add_process_by_file(struct mond * m, char * pidfile)
{
	FILE *f;
	int pid = 0;

	f = fopen(pidfile, "r");
	if (f == NULL)
	{
		printf("ERROR: Unable to open pidfile %s\n", pidfile);
		return ERROR;
	}

	if (fscanf(f, "%d", &pid) == 1)
	{
		fclose(f);
		return mond_add_process(m, pid);
	}
	else
	{
		printf("ERROR: Failed to read pid from pidfile %s\n", pidfile);
		fclose(f);
		return ERROR;
	}
}

int mond_add_process_by_name(struct mond * m, char * name)
{
	char pidfile[50];
	sprintf(pidfile, "/var/run/%s.pid", name);

	return mond_add_process_by_file(m, pidfile);
}

void mond_set_refresh_period(struct mond * m, char * period_str)
{
	float period = atof(period_str); // Refresh rate in seconds

	if (period > 0.5 && period < 3600)
	{
		m->refresh_period = (unsigned long) (period * 1000000);
//		printf("new rate %lu\n", m->refresh_period);
	}
	else
		printf("ERROR: Invalid refresh period %f\n", period);
}

void mond_set_output_status_file(struct mond * m, char * file)
{
	strcpy(m->status_file, file);
}

void get_date_string(char * date_str, int strsz)
{
	time_t now = time(NULL);
	struct tm *t = localtime(&now);

	strftime(date_str, strsz-1, "%T %Y-%m-%d", t);
}

void clean_status_file(struct mond * m)
{
	FILE *f;
	char date[100];
	get_date_string(date, 100);

	f = fopen(m->status_file, "w");
	if (f != NULL)
	{
		fprintf(f, "\n");
		fprintf(f, "Mond has stopped\t\t\t%s\n", date);
		fprintf(f, "\n");

		fclose(f);
	}
}

void generate_status_file(struct mond * m)
{
	FILE *f;
	char date[100];
	get_date_string(date, 100);

	f = fopen(m->status_file, "w");
	if (f != NULL)
	{
		struct process * p = m->process;

		fprintf(f, "\n");
		fprintf(f, "Mond status\t\t\t%s\n", date);
		fprintf(f, "\n");
		fprintf(f, "Current system utilization is %.2f %%\n", m->util.util);

		if (p != NULL)
		{
			fprintf(f, "\n");
			fprintf(f, "Monitored processes\n");
			fprintf(f, "\n");
			fprintf(f, "Name           \tpid    \tstart time\tUtil\tUtil (curr)\n");
		}

		while (p != NULL)
		{
			fprintf(f, "%-15s\t%5d\t%10lu\t%4.2f%%\t%10.2f%%\n", p->name, p->pid, (unsigned long)p->starttime, p->total_util, p->curr_util);
			p = p->next;
		}

		fprintf(f, "\n");
		fprintf(f, "\n");
		fprintf(f, "SEGWAY angle: %.2f\n", m->segway_angle);

		fclose(f);
	}
}

void perform_proc_readings(struct mond * m, unsigned long uptime)
{
	struct process * p = m->process;

	while (p != NULL)
	{
		process_update_util(p, m->util.sys_hertz, uptime);
		p = p->next;
	}

	update_sys_util(&m->util);
}


void update_sys_util(struct utilization * u)
{
	char field[30];
	struct procfile pf;
    unsigned long tmp, idle = 0, total = 0;
    int fno;

	if (procfile_init(&pf, "/proc/stat") == ERROR)
	{
		u->util = 0;
		return;
	}

	for (fno = 2; fno < 11; fno++)
	{
		if (procfile_get_field_from_line(&pf, fno, field) == ERROR)
		{
			printf("ERROR: Cannot get field %d from %s\n", fno, pf.line);
			u->util = 0;
			return;
		}

		tmp = atol(field);
		total += tmp;
		if (fno == 5)
			idle = tmp;
	}

    if (u->first_call)
    	// Reset first call flag for next time
    	u->first_call = 0;
    else
    {
    	/* Compute utilization */
    	u->util = 100.0 - ((100.0 * (idle - u->old_idle)) / (total - u->old_total));
    	//printf("Utilization is: %.2f %%\n", u->util);
    }

    u->old_total = total;
    u->old_idle = idle;

    procfile_cleanup(&pf);

	return;
}


void mond_cleanup(struct mond * m)
{
	printf("mond - cleaning up\n");
	while (m->process != NULL)
	{
		struct process * p = m->process;
		m->process = m->process->next;
		free(p);
	}

	clean_status_file(m);
}


void mond_teardown(struct mond * m)
{
	m->quit_threads = 1;
}


void sig_handler(int signo)
{
    if (signo == SIGINT || signo == SIGTERM)
    {
    	printf("mond caught %s - tearing down\n", (signo == SIGINT ? "SIGINT" : "SIGTERM"));
    	mond_teardown(_mond);
    }
}
