/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "mond-generic.h"
#include "procfile.h"
#include "process.h"

unsigned long long get_starttime(struct process * p)
{
	char start_time_str[30];
	unsigned long start_time = 0;
	struct procfile pf;

	if (procfile_init(&pf, p->stat_path) == ERROR)
		return 0;

	if (procfile_get_field_from_line(&pf, 22, start_time_str) == OK)
		start_time = atol(start_time_str);

	procfile_cleanup(&pf);
//	printf("start time : %lu\n", start_time);
	return start_time;
}


void process_set_name(struct process * p)
{
	char field[30];
	struct procfile pf;

	if (procfile_init(&pf, p->stat_path) == ERROR)
		return;

	if (procfile_get_field_from_line(&pf, 2, field) == OK)
		strcpy(p->name, field);

	procfile_cleanup(&pf);
}


int process_init(struct process * p, int pid)
{
	unsigned long long start_time = 0;
	sprintf(p->stat_path, "/proc/%d/stat", pid);

	if(!(start_time = get_starttime(p)))
		// Something wrong with pid
		return ERROR;
	else
	{
		p->pid = pid;
		p->starttime = start_time;
		p->next = NULL;

		p->tlast.tv_sec = 0;
		p->tlast.tv_usec = 0;
		process_set_name(p);
		return OK;
	}
}

int update_proc_times(struct process * p)
{
	char field[30];
	struct procfile pf;

	if (procfile_init(&pf, p->stat_path) == ERROR)
		return 0;

	if (procfile_get_field_from_line(&pf, 14, field) == OK)
		p->utime = atol(field);
	else
		return ERROR;

	if (procfile_get_field_from_line(&pf, 15, field) == OK)
		p->stime = atol(field);
	else
		return ERROR;

	if (procfile_get_field_from_line(&pf, 16, field) == OK)
		p->cutime = atol(field);
	else
		return ERROR;

	if (procfile_get_field_from_line(&pf, 17, field) == OK)
		p->cstime = atol(field);
	else
		return ERROR;

	procfile_cleanup(&pf);
	return OK;
}

int process_update_util(struct process * p, unsigned long hertz, unsigned long long uptime)
{
	struct timeval tnow;
	unsigned long long total_time, proc_life_time;
	double elapsed_time = 0.0;

	if (update_proc_times(p) == ERROR)
		return ERROR;

	total_time = p->utime + p->stime + p->cutime + p->cstime;

	proc_life_time = uptime - (p->starttime / hertz);     // elapsed time since process started
	p->total_util = 100 * ((total_time / hertz) / (1.0 * proc_life_time));

	//printf ("total util for %d is %f\n", p->pid, p->total_util);

	// Get elapsed time in seconds since last update
	gettimeofday(&tnow, NULL);
	if (p->tlast.tv_sec == 0 && p->tlast.tv_usec == 0)
		elapsed_time = 0.0;
	else
	{
		elapsed_time = (tnow.tv_sec - p->tlast.tv_sec);
		elapsed_time += (tnow.tv_sec - p->tlast.tv_usec) / 1000000.0;   // us to s
		p->curr_util = (total_time - p->last_total_time) / (elapsed_time * hertz);
	//	printf ("curr util for %s (%d) is %.2f\n", p->name, p->pid, p->curr_util);
	}
	p->tlast = tnow;

	return OK;
}
