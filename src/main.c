/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "mond.h"

int main(int argc, char **argv)
{
   	printf("This is mond version %s\n", MOND_VERSION);
   	int i, res = OK;
    struct mond m;

    mond_init(&m);

    for (i = 1; i < argc; ++i)
    {
       	if (strcmp("--help", argv[i]) == 0 || strcmp("-h", argv[i]) == 0 )
       	{
       		printf("Usage: mond [-s <sockpath>][-p <pid>][-pf <pidfile>][-pn <name>]\n");
       		printf("            [-r <refresh_rate>][-f <status_file>]\n");
			res = 1;
			break;
       	}
       	else if (strcmp("-s", argv[i]) == 0)
       	{
       		// Set socket name used to communicate with FreeRTOS cell
       	   	char path[100];
       		strcpy(path, argv[++i]);
       		printf("Using sockpath %s\n", path);
       		mond_set_sockpath(&m, path);
       	}
       	else if (strcmp("-f", argv[i]) == 0)
       	{
       		// Set status_file name used to output monitoring data
       		char status_file[100];
       		strcpy(status_file, argv[++i]);
       		printf("Using output status file %s\n", status_file);
       		mond_set_output_status_file(&m, status_file);
       	}
       	else if (strcmp("-p", argv[i]) == 0)
       	{
       		// Set process to monitor using pid value
       		char pidstr[10];
       		strcpy(pidstr, argv[++i]);
       		mond_add_process_by_pidstr(&m, pidstr);
       	}
       	else if (strcmp("-pf", argv[i]) == 0)
       	{
       		// Set process to monitor using pid file path
       		char pidfile[10];
       		strcpy(pidfile, argv[++i]);
       		mond_add_process_by_file(&m, pidfile);
       	}
       	else if (strcmp("-pn", argv[i]) == 0)
       	{
       		// Set process to monitor using an expected name of a pid file /var/run/<name>.pid
       		char name[20];
       		strcpy(name, argv[++i]);
       		mond_add_process_by_name(&m, name);
       	}
       	else if (strcmp("-r", argv[i]) == 0)
       	{
       		// Set refresh rate in seconds
       		char period[10];
       		strcpy(period, argv[++i]);
       		mond_set_refresh_period(&m, period);
       	}
       	else /* default: */
       	{
       		printf("Unhandled option %s\n", argv[i]);
       		res = ERROR;
       		break;
       	}
    }

    if (res != OK)
    {
       	mond_cleanup(&m);
       	return res;
    }

   	mond_start(&m);
   	mond_cleanup(&m);
   	return OK;
}
