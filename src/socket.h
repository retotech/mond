/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/

#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/un.h>

typedef enum {
	SOCK_NOT_CREATED = 0,
	SOCK_CREATED,
	SOCK_CONNECTED,
} sockstatus;

struct socket {
	int sockfd;
	char sockpath[100];
	sockstatus status;

};

int socket_init(struct socket *, char *);
int socket_connect(struct socket *);
void socket_teardown(struct socket *);
int socket_status(struct socket *);
int socket_receive(struct socket *, char *, int);


#endif /* _SOCKET_H_ */
