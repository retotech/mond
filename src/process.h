/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/

#ifndef PROCESS_H_
#define PROCESS_H_

struct process
{
	char stat_path [50];
	char name [30];
	int pid;
	unsigned long utime; 	// field 14 in /proc/$pid/stat
	unsigned long stime; 	// field 15 in /proc/$pid/stat
	unsigned long cutime; 	// field 16 in /proc/$pid/stat
	unsigned long cstime; 	// field 17 in /proc/$pid/stat
	unsigned long long starttime; // field 22 in /proc/$pid/stat
	unsigned long long last_total_time; // total spent time @ last measurement
	struct timeval tlast;   // wall time at last measurement

	float total_util;		// util since start
	float curr_util;		// util since last computation

	int first_call;
	struct process * next;
};

int process_init(struct process *, int);
int process_update_util(struct process * p, unsigned long hertz, unsigned long long uptime);
void process_set_name(struct process * p);

#endif /* PROCESS_H_ */
