/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#ifndef SRC_MESSAGE_H_
#define SRC_MESSAGE_H_

#include "mond-generic.h"

typedef enum
{
	BADTAG = -1, ANGLE
} tagid_t;

typedef struct
{
	char * tag;
	tagid_t id;
} msg_t;


tagid_t message_id_from_tag(char * tag);

#endif /* SRC_MESSAGE_H_ */
