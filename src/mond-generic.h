/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#ifndef MOND_GENERIC_H_
#define MOND_GENERIC_H_


#define MOND_VERSION "1.0.0"
#define OK 0
#define ERROR -4711
#define MAX_MSG_LEN 128

#define handle_error_en(en, msg) \
	do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)


#endif /* MOND_GENERIC_H_ */
