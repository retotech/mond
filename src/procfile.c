/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#include "mond-generic.h"
#include "procfile.h"

int procfile_init(struct procfile * pf, const char * file)
{
	strcpy(pf->path, file);
	pf->f = fopen(pf->path, "r");
	pf->line = NULL;
	pf->sz = 0;

	if (pf->f == NULL)
	{
		printf("ERROR: Unable to open file %s\n", pf->path);
		return ERROR;
	}

	return procfile_next_line(pf);
}

void procfile_cleanup(struct procfile * pf)
{
	if (pf->line)
	{
		free (pf->line);
		pf->line = NULL;
	}

	if (pf->f)
		fclose(pf->f);

}

int procfile_get_field_from_line(struct procfile * pf, int field_no, char * field)
{
	char * tok;
    int i = 0;
    char * local_line = (char*)calloc(pf->sz, sizeof(char));
    char * iter;

    /* We use a local line that is OK to overwrite with strsep
     * In this way it is OK to call this method multiple times using the same
     * pf->line */
    strncpy(local_line, pf->line, pf->lsz);
    iter = local_line;
//    stringp = &iter;

    while((tok = strsep(&iter," ")) != NULL)
    {
    	/* Only count entries that are not empty strings */
    	if (*tok != '\0')
    		i++;

    	if (i==field_no)
    	{
    		strcpy(field, tok);
    		free(local_line);
    		return OK;
    	}
    	else
    		continue;
    }

    free(local_line);

    // If we reach here we never got to field field_no
    printf("ERROR: We never reached field %d\n", field_no);
    return ERROR;
}


int procfile_next_line(struct procfile * pf)
{
	if (pf->f == NULL)
	{
		printf("ERROR: File not opened (%s)\n", pf->path);
		return ERROR;
	}

	pf->lsz = getline (&pf->line, &pf->sz, pf->f);

	if (pf->lsz < 0)
	{
		printf("ERROR: Cannot read line from file %s\n", pf->path);
		return ERROR;
	}

	return OK;
}

