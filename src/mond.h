/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#ifndef MOND_H_
#define MOND_H_

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>

#include "mond-generic.h"
#include "process.h"
#include "socket.h"

struct freertos_cell
{
	/* Socket comm */
	int listenfd;
	int connfd;
	char sockpath[100]; /* Only for AF_UNIX sockets */

	/* Thread flags */
	int quit_threads;
	int rx_thread_running;
	int tx_thread_running;

	struct module_mgr * next;
};

struct utilization
{
	long sys_hertz;		// Number of ticks per second [Hz]
	unsigned long old_total;
	unsigned long old_idle;
	float util;
	struct timeval tlast;
	int first_call;
};

struct mond
{
	int quit_threads;
	unsigned long refresh_period; // In micro seconds
	char sock_file[100];
	char status_file[100];
	struct utilization util;
	struct process * process;
	struct socket socket;
	char cut_msg[MAX_MSG_LEN];

	float segway_angle;
};


/*
 * mond_start(void)
 * start mond
 */
void mond_start(struct mond *);
void mond_init(struct mond *);
void mond_cleanup(struct mond *);
int mond_add_process(struct mond *, int);
int mond_add_process_by_pidstr(struct mond *, char*);
int mond_add_process_by_file(struct mond *, char*);
int mond_add_process_by_name(struct mond *, char*);
void mond_set_sockpath(struct mond * m, char * sock);
void mond_set_refresh_period(struct mond *, char *);
void mond_set_output_status_file(struct mond *, char *);
void mond_handle_messages(struct mond * m, char * strbuf, int nread);
int mond_act_on_message(struct mond * m, char * msg);

#endif /* STREAMD_H_ */
