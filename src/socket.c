/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>

#include "mond.h"
#include "socket.h"


int socket_init(struct socket * s, char * sockpath)
{
    socket_teardown(s);

    strcpy(s->sockpath, sockpath);
    s->status = SOCK_NOT_CREATED;

	 /* Create socket */
    s->sockfd = socket(AF_UNIX, SOCK_STREAM, 0);

    if (s->sockfd < 0)
    {
    	perror("socket_init: ERROR on socket creation");
    	return ERROR;
    }

    s->status = SOCK_CREATED;
    return OK;
}

int socket_connect(struct socket * s)
{
	int err, len;
    struct sockaddr_un serv_addr;

    if (s->status == SOCK_NOT_CREATED)
    {
    	perror("socket_connect: socket is not available");
    	return ERROR;
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));

    serv_addr.sun_family = AF_UNIX;
    strncpy(serv_addr.sun_path, s->sockpath, sizeof(serv_addr.sun_path));

    len = strlen(serv_addr.sun_path) + sizeof(serv_addr.sun_family);
    err = connect(s->sockfd, (struct sockaddr *)&serv_addr, len);

    if (err < 0)
	{
		perror("socket_connect: ERROR on socket connect");
		return ERROR;
    }

    s->status = SOCK_CONNECTED;
    return SOCK_CONNECTED;
}

int socket_receive(struct socket * s, char * buf, int len)
{
	struct pollfd ufds[] = {
			{
					.fd = s->sockfd,
					.events = POLLIN // | POLLOUT
			}
	};

	if (s->status != SOCK_CONNECTED)
		return 0;

	int rv = poll(ufds, sizeof(ufds) / sizeof(ufds[0]), 0);

	switch (rv) {
	case -1:
		perror("poll");
		return -1;

	case 0:
		//printf("rv:%d - Timeout\n", rv);
		return 0;

	default:
	{
//		printf("socket_receive: rv %d\n", rv);

		if (ufds[0].revents & POLLIN)
		{
//			printf("%s:%d - Ready to read from ufds[0].fd:%d\n", __FILE__, __LINE__, ufds[0].fd);

			ssize_t bytes = recv(ufds[0].fd, buf, len, 0);

			if (0 == bytes)
			{
//				printf("%s:%d - Peer hung up\n", __FILE__, __LINE__);
				socket_teardown (s);
				return 0;
			}

//			printf("%s:%d - Read %ld bytes from ufds[0].fd:%d.\n", __FILE__, __LINE__, bytes, ufds[0].fd);
			return bytes;
		}
		return 0;
	} // default:
	} // switch (rv)
}

int socket_send(struct socket * s, char * str)
{
	/*
	 *
		if (ufds[0].revents & POLLOUT)
		{
			fprintf(stdout,
					"%s:%d - Ready to write to ufds[0].fd:%d.\n",
					__FILE__, __LINE__, ufds[0].fd);
			char puf[] = "Hello from icctpump";
			size_t len = strlen(puf) + 1;
			ssize_t n = send(ufds[0].fd, puf, sizeof(puf), 0);

			if (0 > n)
			{
				perror("send");
				return 0;
			}

			fprintf(stdout,
					"%s:%d - Done writing (%ld bytes) to ufds[0]\n",
					__FILE__, __LINE__, n);
			ufds[0].events &= ~POLLOUT;
		}

	 *
	 */

	return 0;
}

int socket_status(struct socket * s)
{
	return s->status;
}


void socket_teardown(struct socket * s)
{
	if (s->sockfd >= 0)
	{
		close(s->sockfd);
		s->sockfd = -1;
	}
	s->status = SOCK_NOT_CREATED;
}

