/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/
#include <string.h>
#include "message.h"


msg_t msgtable[] = {
		{ "segway_angle", ANGLE },
};

#define NTAGS (sizeof(msgtable)/sizeof(msg_t))


tagid_t message_id_from_tag(char * tag)
{
	unsigned int i;
	msg_t * msgtype = msgtable;
	for (i=0; i < NTAGS; i++, msgtype++)
	{
		if (strcmp(msgtype->tag, tag) == 0)
			return msgtype->id;
	}
	return BADTAG;
}
