/* 
* mond, monitoring daemon 
* 
* Copyright (c) Retotech AB, 2016-2017 
* 
* Authors: 
* Ola Redell <ola.redell@retotech.se> 
* 
* This work is licensed under the terms of the MIT license. See 
* the COPYING file in the top-level directory.
*/

#ifndef PROCFILE_H_
#define PROCFILE_H_

#include <string.h>
#include <stdio.h>
#include <malloc.h>

struct procfile
{
	char path[100];
	FILE *f;
	char * line;
	size_t sz;   /* size of line buffer */
	ssize_t lsz; /* length of "line string" */
};

int procfile_init(struct procfile * pf, const char * file);
void procfile_cleanup(struct procfile * pf);
int procfile_get_field_from_line(struct procfile * pf, int field_no, char * field);
int procfile_next_line(struct procfile * pf);

#endif /* PROCFILE_H_ */
